﻿using FCODE.DEMO.Core;
using FCODE.DEMO.DATA.Entity;
using FCODE.DEMO.DATA.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCODE.DEMO.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new SqliteDBContext())
            {
                //查看初始化数据
                Console.WriteLine(db.LocalPerson.ToList().ToJson());

                //添加数据
                db.LocalPerson.Add(new LocalPerson()
                {
                    Name = "测试添加",
                    Sex = 2
                });
                db.SaveChanges();

                //查看总数居
                Console.WriteLine(db.LocalPerson.ToList().ToJson());
            }
        }
    }
}
