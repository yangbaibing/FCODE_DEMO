﻿using FCODE.CORE.WPFControls.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCODE.DEMO.WPF
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var source = new List<KeyValuePair<string, string>>();
            for (int i = 1; i <= 10; i++)
            {
                source.Add(new KeyValuePair<string, string>("Key" + i, "Value" + i));
            }
            source.Add(new KeyValuePair<string, string>("长长长长长长长长长长长长长长长长长长长长长", "Value"));
            cbbTest.ItemsSource = source;

            var index = 1;
            //设置数据源
            mtcbbTest.SetSource(source.Select(x => new CORE.WPFControls.Model.MultiComboBoxItem { Text = x.Key, Value = x.Value, IsChecked = index++ <= 3 }).ToList());
            mtcbbTestVertical.SetSource(source.Select(x => new CORE.WPFControls.Model.MultiComboBoxItem { Text = x.Key, Value = x.Value, IsChecked = index++ <= 12 }).ToList());

            var tree = new List<TreeSelectItem>();
            for (int i = 1; i <= 10; i++)
            {
                tree.Add(new TreeSelectItem()
                {
                    Text = "Text" + i,
                    Value = "Value" + i
                });
            }
            tree[0].IsExpanded = true;
            tree[0].Children = new ObservableCollection<TreeSelectItem>(new List<TreeSelectItem>(tree[0].Level) {
                new TreeSelectItem { Text = "Text1_1_长文本长文本长文本长文本长文本",
                    Value = "test",
                    IsChecked = true,
                    IsSelected = true ,
                Children=new ObservableCollection<TreeSelectItem>
                {
                    new TreeSelectItem(1){
                        Text="test",
                        Value=1
                    }
                } } });
            tsTest.ItemsSource = new ObservableCollection<TreeSelectItem>(tree);

            mtsTest.SetSource(new ObservableCollection<TreeSelectItem>(tree));
        }

        private void btnViewMultiChecked_Click(object sender, RoutedEventArgs e)
        {
            var tips = string.Join(",", mtcbbTest.CheckedItems.Select(x => x.Text).ToList());
            MessageBox.Show(tips);
        }
    }
}
