﻿using FCODE.DEMO.DATA.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCODE.DEMO.DATA.EntityFramework
{
    public class SqliteDBContext : DbContext
    {
        public SqliteDBContext() : base("name = localdb")
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            Database.SetInitializer(new LocalDbInitializer(modelBuilder));
        }

        public DbSet<LocalPerson> LocalPerson { get; set; }
    }
}
