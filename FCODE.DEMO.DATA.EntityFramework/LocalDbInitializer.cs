﻿#region 
/* 
 *  FileName: LocalDbInitializer
 *  Author: 杨白兵
 *  Created: 2018/11/12 11:44:23
 *  Description:
 */
#endregion

using SQLite.CodeFirst;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCODE.DEMO.DATA.EntityFramework
{
    public class LocalDbInitializer : SqliteDropCreateDatabaseWhenModelChanges<SqliteDBContext>
    {
        public LocalDbInitializer(DbModelBuilder modelBuilder) : base(modelBuilder)
        {
        }

        protected override void Seed(SqliteDBContext context)
        {
            //初始参数
            context.LocalPerson.Add(new Entity.LocalPerson
            {
                Name = "初始化测试",
                Sex = 1
            });
            context.SaveChanges();

            base.Seed(context);
        }
    }
}
