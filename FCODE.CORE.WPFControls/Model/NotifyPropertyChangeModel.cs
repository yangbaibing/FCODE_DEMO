﻿#region 
/* 
 *  FileName: NotifyPropertyChangeModel
 *  Author: 杨白兵
 *  Created: 2018/11/20 15:49:10
 *  Description:
 */
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCODE.CORE.WPFControls.Model
{
    public abstract class NotifyPropertyChangeModel: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
