﻿#region 
/* 
 *  FileName: TreeSelectItem
 *  Author: 杨白兵
 *  Created: 2018/11/22 11:07:57
 *  Description:
 */
#endregion

using System.Collections.ObjectModel;

namespace FCODE.CORE.WPFControls.Model
{
    public class TreeSelectItem : MultiComboBoxItem
    {
        public TreeSelectItem(int _parentLevel = 0)
        {
            Level = _parentLevel + 1;
        }

        private bool _IsSelected;
        /// <summary>
        /// 是否选中(TreeSelect 绑定数据时作为已选中项）
        /// </summary>
        public bool IsSelected
        {
            get { return _IsSelected; }
            set
            {
                _IsSelected = value;
                RaisePropertyChanged("IsSelected");
            }
        }

        private bool _IsEnabled = true;
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsEnabled
        {
            get { return _IsEnabled; }
            set
            {
                _IsEnabled = value;
                RaisePropertyChanged("_IsEnabled");
            }
        }

        private int _Level;
        /// <summary>
        /// 节点等级
        /// </summary>
        public int Level
        {
            get { return _Level; }
            set
            {
                _Level = value;
                RaisePropertyChanged("Level");
            }
        }

        private bool _IsExpanded;
        /// <summary>
        /// 是否展开
        /// </summary>
        public bool IsExpanded
        {
            get { return _IsExpanded; }
            set
            {
                _IsExpanded = value;
                RaisePropertyChanged("IsExpanded");
            }
        }

        private ObservableCollection<TreeSelectItem> _children;
        /// <summary>
        /// 子集
        /// </summary>
        public ObservableCollection<TreeSelectItem> Children
        {
            get { return _children; }
            set
            {
                _children = value;
                RaisePropertyChanged("Children");
            }
        }

    }
}
