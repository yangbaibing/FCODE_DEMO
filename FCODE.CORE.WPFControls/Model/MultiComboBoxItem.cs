﻿#region 
/* 
 *  FileName: ItemModel
 *  Author: 杨白兵
 *  Created: 2018/11/20 11:00:26
 *  Description:
 */
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCODE.CORE.WPFControls.Model
{
    public class MultiComboBoxItem : NotifyPropertyChangeModel
    {
        private string _Text;
        /// <summary>
        /// Text
        /// </summary>
        public string Text
        {
            get { return _Text; }
            set
            {
                _Text = value;
                RaisePropertyChanged("Text");
            }
        }


        private object _Value;
        /// <summary>
        /// Value
        /// </summary>
        public object Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
                RaisePropertyChanged("Value");
            }
        }

        private object _Ext;
        /// <summary>
        /// 扩展字段
        /// </summary>
        public object Ext
        {
            get { return _Ext; }
            set
            {
                _Ext = value;
            }
        }


        private bool _IsChecked;
        /// <summary>
        /// 是否选中
        /// </summary>
        public bool IsChecked
        {
            get { return _IsChecked; }
            set
            {
                _IsChecked = value;
                RaisePropertyChanged("IsChecked");
            }
        }
    }
}
