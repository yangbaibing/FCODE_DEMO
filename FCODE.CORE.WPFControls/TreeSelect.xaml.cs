﻿using FCODE.CORE.WPFControls.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCODE.CORE.WPFControls
{
    /// <summary>
    /// TreeSelect.xaml 的交互逻辑
    /// </summary>
    public partial class TreeSelect : System.Windows.Controls.ComboBox, INotifyPropertyChanged
    {
        public TreeSelect()
        {
            InitializeComponent();

            Loaded += TreeSelect_Loaded;
        }

        private TreeView _tvItems;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _tvItems = Template.FindName("tvItems", this) as TreeView;
        }

        /// <summary>
        /// 控件加载完成
        /// </summary>
        private void TreeSelect_Loaded(object sender, RoutedEventArgs e)
        {
            if (ItemsSource != null)
                GetTreeRecursionSelected(ItemsSource as ObservableCollection<TreeSelectItem>);
        }

        #region Field & Property
        /// <summary>
        /// 选中项列表
        /// </summary>
        private TreeSelectItem SelecteTreeItemProperty = new TreeSelectItem();
        [Bindable(true)]
        public TreeSelectItem SelecteTreeItem
        {
            get { return SelecteTreeItemProperty; }
            set
            {
                SelecteTreeItemProperty = value;
                DisplayText = SelecteTreeItemProperty.Text;
            }
        }

        private string _Text = string.Empty;
        [Bindable(true)]
        public string DisplayText
        {
            get { return _Text; }
            set
            {
                _Text = value;
                RaisePropertyChanged("DisplayText");
            }
        }

        private bool _IsExpandedAll = false;
        /// <summary>
        /// 是否全部展开
        /// </summary>
        [Bindable(true)]
        public bool IsExpandedAll
        {
            get { return _IsExpandedAll; }
            set
            {
                _IsExpandedAll = value;
                RaisePropertyChanged("IsExpandedAll");
            }
        }

        #endregion

        #region Method
        /// <summary>
        /// 展开指定节点
        /// </summary>
        /// <param name="container"><see cref="TreeViewItem"/></param>
        public void ExpandSubtree(TreeViewItem container)
        {
            _tvItems.ExpandNode(container);
        }
        #endregion

        #region Event
        public delegate void SelectedItemChangeHandle(object sender, RoutedPropertyChangedEventArgs<object> e);
        /// <summary>
        /// 选中前事件
        /// </summary>
        public event SelectedItemChangeHandle PreviewSelectedItemChange;

        /// <summary>
        /// 选中事件
        /// </summary>
        public event SelectedItemChangeHandle SelectedItemChanged;

        /// <summary>
        /// 设置数据源
        /// </summary>
        /// <param name="source"></param>
        public void SetSource(ObservableCollection<TreeSelectItem> source)
        {
            ItemsSource = source;

            GetTreeRecursionSelected(source);
        }

        /// <summary>
        /// 获取已选中项
        /// </summary>
        /// <param name="tree"></param>
        void GetTreeRecursionSelected(ObservableCollection<TreeSelectItem> tree)
        {
            foreach (var item in tree)
            {
                if (item.IsSelected)
                {
                    SelecteTreeItem = item;
                    return;
                }

                if (item.Children != null && item.Children.Any())
                    GetTreeRecursionSelected(item.Children);
            }
        }

        /// <summary>
        /// 选中项变更
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tvItems_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            //选中前事件
            if (PreviewSelectedItemChange != null)
            {
                PreviewSelectedItemChange(sender, e);
                if (e.Handled == true)
                    return;
            }
            var item = (TreeSelectItem)e.NewValue;
            if (item == null) return;

            SelecteTreeItem = item;

            if (SelectedItemChanged != null)
                SelectedItemChanged(sender, e);
        }
        #endregion

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }
}
