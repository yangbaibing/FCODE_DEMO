﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCODE.CORE.WPFControls
{
    /// <summary>
    /// TreeVIew.xaml 的交互逻辑
    /// </summary>
    public partial class TreeView : System.Windows.Controls.TreeView, INotifyPropertyChanged
    {
        public TreeView()
        {
            InitializeComponent();
        }

        [Bindable(true)]
        public bool IsMultiChecked
        {
            get { return (bool)GetValue(IsMultiCheckedProperty); }
            set
            {
                SetValue(IsMultiCheckedProperty, value);
            }
        }

        public static readonly DependencyProperty IsMultiCheckedProperty =
            DependencyProperty.Register("IsMultiChecked",
                typeof(bool),
                typeof(TreeView),
                new PropertyMetadata(false));


        [Bindable(true)]
        public bool IsExpandedAll
        {
            get { return (bool)GetValue(IsExpandedAllProperty); }
            set
            {
                SetValue(IsExpandedAllProperty, value);
            }
        }

        public static readonly DependencyProperty IsExpandedAllProperty =
            DependencyProperty.Register("IsExpandedAll",
                typeof(bool),
                typeof(TreeView),
                new PropertyMetadata(false));

        /// <summary>
        /// 展开指定节点
        /// </summary>
        /// <param name="container"><see cref="TreeViewItem"/></param>
        public void ExpandNode(TreeViewItem container)
        {
            this.ExpandSubtree(container);
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
