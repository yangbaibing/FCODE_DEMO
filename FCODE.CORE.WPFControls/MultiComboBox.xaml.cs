﻿using FCODE.CORE.WPFControls.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCODE.CORE.WPFControls
{
    /// <summary>
    /// MultiComboBox.xaml 的交互逻辑
    /// </summary>
    public partial class MultiComboBox : System.Windows.Controls.ComboBox, INotifyPropertyChanged
    {
        public MultiComboBox()
        {
            InitializeComponent();
        }

        #region Fields

        /// <summary>
        /// 选中项列表
        /// </summary>
        private ObservableCollection<MultiComboBoxItem> CheckedItemsProperty = new ObservableCollection<MultiComboBoxItem>();

        [Bindable(true)]
        public ObservableCollection<MultiComboBoxItem> CheckedItems
        {
            get { return CheckedItemsProperty; }
            set
            {
                CheckedItemsProperty = value;
                RaisePropertyChanged("CheckedItems");
            }
        }

        /// <summary>
        /// 展示列表
        /// </summary>
        private ObservableCollection<MultiComboBoxItem> MultiItemSourceProperty = new ObservableCollection<MultiComboBoxItem>();

        [Bindable(true)]
        public ObservableCollection<MultiComboBoxItem> MultiItemSource
        {
            get { return MultiItemSourceProperty; }
            set
            {
                MultiItemSourceProperty = value;
                RaisePropertyChanged("MultiItemSource");
            }
        }

        [Bindable(true)]
        public Orientation CheckedOrientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set
            {
                SetValue(OrientationProperty, value);
            }
        }

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("CheckedOrientation",
                typeof(Orientation),
                typeof(MultiComboBox),
                new PropertyMetadata(Orientation.Horizontal));
        #endregion

        #region Method

        /// <summary>
        /// 设置数据源
        /// </summary>
        /// <param name="source"></param>
        public void SetSource(IEnumerable<MultiComboBoxItem> source)
        {
            MultiItemSource = new ObservableCollection<MultiComboBoxItem>(source);

            if (MultiItemSource.Any(x => x.IsChecked))
                CheckedItems = new ObservableCollection<MultiComboBoxItem>(source.Where(x => x.IsChecked).ToList());
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

        #region Event

        /// <summary>
        /// 下拉列表选中变更
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListBoxItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (var item in e.AddedItems)
            {
                MultiComboBoxItem datachk = item as MultiComboBoxItem;
                datachk.IsChecked = true;
                if (CheckedItems.IndexOf(datachk) < 0)
                    CheckedItems.Add(datachk);
            }

            foreach (var item in e.RemovedItems)
            {
                MultiComboBoxItem datachk = item as MultiComboBoxItem;
                datachk.IsChecked = false;
                CheckedItems.Remove(datachk);
            }
        }

        /// <summary>
        /// 复选框取消选中
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            var ckb = (CheckBox)sender;
            ListBoxItem cp = ckb.TemplatedParent as ListBoxItem;
            MultiComboBoxItem item = cp.Content as MultiComboBoxItem;
            item.IsChecked = false;
            CheckedItems.Remove(item);
        }
        #endregion
    }
}
