﻿using FCODE.CORE.WPFControls.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FCODE.CORE.WPFControls
{
    /// <summary>
    /// MultiTreeSelect.xaml 的交互逻辑
    /// </summary>
    public partial class MultiTreeSelect : System.Windows.Controls.ComboBox, INotifyPropertyChanged
    {
        public MultiTreeSelect()
        {
            InitializeComponent();
        }

        #region Field & Property
        private TreeView _tvItems;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            _tvItems = Template.FindName("tvItems", this) as TreeView;
        }

        /// <summary>
        /// 选中项列表
        /// </summary>
        private ObservableCollection<TreeSelectItem> SelectedArrayProperty = new ObservableCollection<TreeSelectItem>();
        [Bindable(true)]
        public ObservableCollection<TreeSelectItem> SelectedArray
        {
            get { return SelectedArrayProperty; }
            set
            {
                SelectedArrayProperty = value;
                RaisePropertyChanged("SelectedArray");
            }
        }

        [Bindable(true)]
        public Orientation SelectedOrientation
        {
            get { return (Orientation)GetValue(OrientationProperty); }
            set
            {
                SetValue(OrientationProperty, value);
            }
        }

        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("SelectedOrientation",
                typeof(Orientation),
                typeof(MultiTreeSelect),
                new PropertyMetadata(Orientation.Horizontal));

        private bool _IsExpandedAll = false;

        [Bindable(true)]
        public bool IsExpandedAll
        {
            get { return _IsExpandedAll; }
            set
            {
                _IsExpandedAll = value;
                RaisePropertyChanged("IsExpandedAll");
            }
        }
        #endregion

        #region Method

        /// <summary>
        /// 展开指定节点
        /// </summary>
        /// <param name="container"><see cref="TreeViewItem"/></param>
        public void ExpandSubtree(TreeViewItem container)
        {
            _tvItems.ExpandNode(container);
        }
        #endregion

        #region Event
        public delegate void CheckBoxHandle(object sender, RoutedEventArgs e);

        /// <summary>
        /// 选中前事件
        /// </summary>
        public event CheckBoxHandle PreviewCheckBoxClick;

        /// <summary>
        /// 选中事件
        /// </summary>
        public event CheckBoxHandle CheckBoxClicked;

        /// <summary>
        /// 移除选中事件
        /// </summary>
        public event CheckBoxHandle RemoveCheckBoxClicked;

        /// <summary>
        /// 初始化
        /// </summary>
        public event CheckBoxHandle TreeSelectInitialized;
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// 加载成功后初始化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            if (TreeSelectInitialized != null)
                TreeSelectInitialized(sender, e);

            if (ItemsSource != null)
                AddTreeRecursionChecked(ItemsSource as ObservableCollection<TreeSelectItem>);
        }
        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// 设置数据源
        /// </summary>
        /// <param name="source"></param>
        public void SetSource(ObservableCollection<TreeSelectItem> source)
        {
            ItemsSource = source;
            SelectedArray.Clear();
            AddTreeRecursionChecked(source);
        }

        /// <summary>
        /// 获取已选中项
        /// </summary>
        /// <param name="tree"></param>
        void AddTreeRecursionChecked(ObservableCollection<TreeSelectItem> tree)
        {
            foreach (var item in tree)
            {
                if (item.IsChecked)
                {
                    SelectedArray.Add(item);
                }

                if (item.Children != null && item.Children.Any())
                    AddTreeRecursionChecked(item.Children);
            }
        }

        /// <summary>
        /// 选择框点击事件
        /// </summary>
        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            var chk = (CheckBox)sender;

            //选中前事件
            if (PreviewCheckBoxClick != null)
            {
                PreviewCheckBoxClick(sender, e);
                if (e.Handled == true)
                {
                    chk.IsChecked = !chk.IsChecked;
                    return;
                }
            }

            var entity = chk.DataContext as TreeSelectItem;

            if (chk.IsChecked == true)
            {
                entity.IsChecked = true;
                //entity.IsSelected = true;
                SelectedArray.Add(entity);
            }
            else
            {
                SelectedArray.Remove(entity);
            }
            //选中事件
            if (CheckBoxClicked != null)
                CheckBoxClicked(this, e);
        }

        /// <summary>
        /// 移除点击
        /// </summary>
        private void chbRemove_Click(object sender, RoutedEventArgs e)
        {
            var chk = (CheckBox)sender;

            var entity = chk.DataContext as TreeSelectItem;
            SelectedArray.Remove(entity);

            TreeRecursion((ObservableCollection<TreeSelectItem>)ItemsSource, entity);

            //选中前事件
            if (RemoveCheckBoxClicked != null)
            {
                e.Source = entity;
                RemoveCheckBoxClicked(this, e);
                if (e.Handled == true)
                {
                    chk.IsChecked = !chk.IsChecked;
                    return;
                }
            }
        }

        /// <summary>
        /// 树数据递归处理
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="entity"></param>
        void TreeRecursion(ObservableCollection<TreeSelectItem> tree, TreeSelectItem entity)
        {
            foreach (var item in tree)
            {
                if (item == entity)
                {
                    item.IsChecked = false;
                    //item.IsSelected = false;
                    return;
                }

                if (item.Children != null && item.Children.Any())
                    TreeRecursion(item.Children, entity);
            }
        }

        #endregion

    }
}
