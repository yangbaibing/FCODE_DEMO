# FCODE_DEMO

#### 项目介绍
以前写的一个工具demo 
- 1、Sqlite EF6 
- 2、WPF 自定义下拉框控件（多选）、下拉树形控件TreeSelect（多项）

#### 软件架构
.net 4.5

#### Sqlie EF6 支持

项目博客介绍：https://blog.csdn.net/Johnson55925/article/details/84140057

#### WPF 自定义控件

- 自定义下拉框选择框

![自定义选择框](https://images.gitee.com/uploads/images/2020/1026/103000_2b4684d2_972538.gif "自定义下拉框选择框,支持多选")

项目博客介绍：https://blog.csdn.net/Johnson55925/article/details/84335250





- 自定义树形下拉选择框

![树形选择](https://images.gitee.com/uploads/images/2020/1026/102430_dd6a9f41_972538.gif "自定义树形下拉选择框，支持多选")

项目博客介绍：https://blog.csdn.net/Johnson55925/article/details/85988891#comments_12244141

