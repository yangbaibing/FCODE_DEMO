﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCODE.DEMO.DATA.Entity
{
    public class LocalPerson
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]//ID 自增长
        public int ID { get; set; }

        public string Name { get; set; }

        public int Sex { get; set; }
    }
}
