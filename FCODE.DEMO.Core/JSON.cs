﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FCODE.DEMO.Core
{
    public static class JSON
    {
        /// <summary>
        ///     json 序列化设置
        /// </summary>
        /// <returns>
        ///     The <see cref="JsonSerializerSettings" />.
        /// </returns>
        public static JsonSerializerSettings SerializerSetting()
        {
            var setting = new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                PreserveReferencesHandling = PreserveReferencesHandling.None,
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore,
                DateFormatString = "yyyy-MM-dd HH:mm:ss",
                ContractResolver = new DefaultContractResolver
                {
                    IgnoreSerializableInterface = true,
                    IgnoreSerializableAttribute = true,
                },
                Error = delegate (object obj, ErrorEventArgs args)
                {
                    //args.ErrorContext.Error.LogError();
                    args.ErrorContext.Handled = true;
                }
            };

            return setting;
        }

        /// <summary>
        /// 转化为json
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToJson(this object obj, string dateFormatString = null)
        {
            if (obj == null)
            {
                return string.Empty;
            }
            var setting = SerializerSetting();
            if (!string.IsNullOrEmpty(dateFormatString))
                setting.DateFormatString = dateFormatString;
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj, setting);
        }

        /// <summary>
        /// 反json为对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T Deserialize<T>(this string json)
        {
            if (string.IsNullOrWhiteSpace(json))
            {
                return default(T);
            }
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }
    }
}
